package com.thoughtworks;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class ExecutorJar {

    public static void startExecutorJar(){
        try{
            Runtime.getRuntime().exec("nohup java -jar device-simulator.jar &");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--ignore-certificate-errors");
            WebDriver driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(500));
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + System.getProperty(File.separator) + "chromedriver");
            String url = "https://pos-uat.sodimac.cl/pos-frontend/";
            try{
                driver.get(url);
                WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), 'Por favor inicie sesión usando ID de cajero')]")));
                Assert.assertEquals(element.getText(),"Por favor inicie sesión usando ID de cajero");
            } catch (Exception e){}

            Thread.sleep(6000);
            productScan("4120101978078");

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='dialog']/form/div/button[@data-testid='8']"))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='dialog']/form/div/button[@data-testid='1']"))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='dialog']/form/div/button[@data-testid='7']"))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='dialog']/form/div/button[@data-testid='1']"))).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@role='dialog']/form/button[@data-testid='submit-button']")));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='dialog']/form/button[@data-testid='submit-button']"))).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='root']/div/section/div/button[text()='Comenzar']")));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='root']/div/section/div/button[text()='Comenzar']"))).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), 'Ya puedes comenzar a escanear tus productos')]")));

            productScan("2000001005460");

            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@data-testid='yes-continue']")));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-testid='yes-continue']"))).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(), 'Pagar con')]")));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(), 'Pagar con')]"))).click();

            String scriptToExecute = "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";
            String netData = ((JavascriptExecutor)driver).executeScript(scriptToExecute).toString();
            System.out.println(netData);
            driver.close();
            driver.quit();
        } catch (InterruptedException | IOException e){
            e.printStackTrace();
        }
    }

    public static void productScan(String code){
        RestAssured.baseURI ="http://localhost:9009/scanner";
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("code", code);
        requestParams.put("type", 101);
        request.body(requestParams.toString());
        request.post();
    }

    @Test
    public void testConnect() {
        startExecutorJar();
    }
}
